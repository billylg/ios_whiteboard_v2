//
//  DrawContext.h
//  Draw2
//
//  Created by Billy Liang on 10/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Draw2ViewController.h"


@interface DrawContext : NSObject {
    NSMutableArray *dataPointArray;
    CGColor_t rgbColor;
}

@property (nonatomic, retain) NSMutableArray *dataPointArray;

-(void) setColorContext:(CGColor_t) colorContext;
-(CGColor_t) getColorContext;

@end
