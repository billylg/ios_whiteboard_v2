//
//  DrawContext.m
//  Draw2
//
//  Created by Billy Liang on 10/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DrawContext.h"

@implementation DrawContext

@synthesize dataPointArray;

-(void) setColorContext:(CGColor_t) colorContext
{
    rgbColor = colorContext;
}

-(CGColor_t) getColorContext
{
    return rgbColor;
}

- (void)dealloc
{
    [dataPointArray release];
    [super dealloc];
}

@end
