//
//  Draw2ViewController.m
//  Draw2
//
//  Created by Billy Liang on 8/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include <stdlib.h>
#import "Draw2ViewController.h"
#import "ASIHTTPRequest.h"
#import "../JSON/SBJson.h"
#import "Draw2View.h"
#import "Draw2InputViewController.h"
#import "DrawContext.h"
#import "Draw2SaveController.h"
#import "CameraPreviewController.h"
#import "ImportViewController.h"

@implementation Draw2ViewController

@synthesize points;
@synthesize data;
@synthesize sessionID;
@synthesize dateTime;
@synthesize inputStream;
@synthesize outputStream;
@synthesize imageOutStream;
@synthesize imageInStream;
@synthesize toServerQ;
@synthesize fromServerQ;
@synthesize userID;
@synthesize serverIP;
@synthesize imageToUpload;
@synthesize rawData;
@synthesize clientHost;
@synthesize markerArray;
@synthesize popOver;
@synthesize lastPointFromUser;
@synthesize connectionButton;
@synthesize cameraButton;
@synthesize saveButton;
@synthesize colorButton;
@synthesize eraseButton;
@synthesize joinStateBuffer;
@synthesize drawingColor;
@synthesize currentColor;
@synthesize activityIndicator;

#define isDEBUG 1  // TODO: remove this flag in the production code

- (void)dealloc
{
    [points release];
    [data release];
    [sessionID release];
    [dateTime release];
    [timer invalidate];
    [fromServerQ release];
    [toServerQ release];
    [lastPointFromUser release];
    [serverIP release];
    [sessionID release];
    [userID release];
    [inputStream close];
    [outputStream close];
    [inputStream release]; // if program crash upon exit, comment out this line
    [outputStream release]; // if program crash upon exit, comment out this line 
    if(markerArray != nil) {
        [markerArray release];
    }
    [joinStateBuffer release];
    [activityIndicator release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)initNetworkCommunication:(NSString *)ip_address withPort:(NSInteger)port role:(network_role_t)role
{
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    NSLog(@"server IP: %@, port: %d, role: %d", ip_address, port, role);
    /* don't need to enable read stream for sending screen shot */
    if (port != 8080) {
        /* schedule the stream socket on main runloop since the thread's runloop is short lived */
        if (role == HOST) {
            CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)ip_address, port, NULL, &writeStream);
            imageOutStream = (NSOutputStream *)writeStream;
            [imageOutStream setDelegate:self];
            [imageOutStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
            [imageOutStream open];
            NSLog(@"host connected");
        } else if (role == CLIENT) {
            CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)ip_address, port, &readStream, NULL);
            imageInStream = (NSInputStream *)readStream;
            [imageInStream setDelegate:self];
            [imageInStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
            [imageInStream open];
            NSLog(@"client connected");
        }
    } else {
        CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)ip_address, port, &readStream, &writeStream);
        inputStream = (NSInputStream *)readStream;
        outputStream = (NSOutputStream *)writeStream;
        [inputStream setDelegate:self];
        [outputStream setDelegate:self];
        [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [inputStream open];
        [outputStream open];
    }
}

//- (void) drawUpdate:(NSMutableArray*)newPoints
- (void) drawUpdate:(DrawContext*)context
{
    Draw2View *drawView = (Draw2View*) self.view;
    [drawView updateView:context.dataPointArray withColor:[context getColorContext]];
}

- (void) initSpinner
{
    /*
    activityIndicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge]
                         autorelease];
    CGPoint newCenter = (CGPoint) [self.view center];
    activityIndicator.center = newCenter;
    [self.view addSubview:activityIndicator];
     */
    [activityIndicator startAnimating];
    activityIndicator.hidden = NO;
}

/* TODO: lots of object not freed in this function 
   make sure to release them during code clean up
 */
- (void) receiveDataFromServer:(NSString *)input
{
    int j, input_len, index = 0, count = 0;
    CGPoint point;
    NSValue *pointValue;
    NSString *userName;
    NSRange search_range, range, segment_range;
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    NSMutableArray *inputArray = [[NSMutableArray alloc] init];
    
    /* task to be run in NSOperation Queue */
    NSLog(@"server said: %@", input);
    input_len = [input length];
    
    //while (range.location != NSNotFound) {
    while (index < input_len) {
        search_range = NSMakeRange(index, input_len-index);
        range = [input rangeOfString:@"\n" options:0 range:search_range];
        if (range.location == NSNotFound && range.length == 0) {
            /* breaks out, weird that we don't find "\n" in the string */
            break;
        } else {
            segment_range = NSMakeRange(index, range.location - index);
            NSString *segment = [[NSString alloc] initWithString:[input substringWithRange:segment_range]];
            NSLog(@"separating into segment\n%@", segment);
            index = range.location + range.length;
            /*if (input_len - index <= 0)
                break;
            search_range = NSMakeRange(index, input_len-index);
             */
            //range = [input rangeOfString:@"\n" options:0 range:search_range];
            [inputArray addObject:segment];
            [segment release];
        } 
    }
    NSLog(@"there are %d segments in the inputArray", [inputArray count]);
    for (NSString *s in inputArray) {
        NSDictionary *dict = (NSDictionary *)[jsonParser objectWithString:s];
        [dict retain]; // TODO: do we need this retain here??
        if ([dict valueForKey:@"request"] != nil) {
            if ([[dict valueForKey:@"request"] isEqualToString:@"getState"]) {
                NSLog(@"received request to upload screen shot to server");
                /* get the port# then create a new socket to upload screen shot */
                if ([dict valueForKey:@"port"] != nil) {
                    NSString *portStr = [dict valueForKey:@"port"];
                    clientHost = [[dict valueForKey:@"clientHost"] retain];
                    clientPort = [[dict valueForKey:@"clientPort"] integerValue];
                    /* the order for creating socket and sending file size to server should not change */
                    [self initNetworkCommunication:self.serverIP withPort:[portStr intValue] role:HOST];
                } else {
                    NSLog(@"ERROR: malformed json string received");
                }
            } else if ([[dict valueForKey:@"request"] isEqualToString:@"bulkJoin"]) {
                clientPort = [[dict valueForKey:@"port"] integerValue];
                fileSize = [[dict valueForKey:@"length"] integerValue];
                app_state = INSTANT_SHARE;
                [self initNetworkCommunication:self.serverIP withPort:clientPort role:CLIENT];
            } else if ([[dict valueForKey:@"request"] isEqualToString:@"ackStartBulkSend"]) {
                clientPort = [[dict valueForKey:@"port"] integerValue];
                [self initNetworkCommunication:self.serverIP withPort:clientPort role:HOST];
            }else if ([[dict valueForKey:@"request"] isEqualToString:@"trash"]) {
                Draw2View *imgView;
                /* clean the canvas, remove all array points and bg image */
                [points removeAllObjects];
                [lastPointFromUser removeAllObjects];
                [markerArray removeAllObjects]; 
                readIndex = 0;
                imgView = (Draw2View *)self.view;
                imgView.image = nil;
                [imgView setNeedsDisplay];
                [CATransaction flush];
            } else if ([[dict valueForKey:@"request"] isEqualToString:@"returnState"]) {
                NSLog(@"received request to open socket to receive screen shot");
                if ([dict valueForKey:@"port"] != nil) {
                    NSString *portStr = [dict valueForKey:@"port"];
                    if ([dict valueForKey:@"length"] != nil) {
                        fileSize = [[dict valueForKey:@"length"] integerValue];
                    }
                    NSLog(@"new server port is %@, received file size: %d", portStr, fileSize);
                    [self performSelectorOnMainThread:@selector(initSpinner) withObject:nil waitUntilDone:NO];
                    /* connect to server to download background image, the order to open socket and send ACK cannot be changed */
                    [self initNetworkCommunication:self.serverIP withPort:[portStr intValue] role:CLIENT];
                } else {
                    NSLog(@"ERROR: malformed json string came in");
                }
            } else if ([[dict valueForKey:@"request"] isEqualToString:@"startSend"] ||
                       [[dict valueForKey:@"request"] isEqualToString:@"sendBulkDataRequest"]) {
                NSData *dataObj = UIImageJPEGRepresentation(imageToUpload, 1.0);
                const uint8_t *buf = [dataObj bytes];
                NSInteger size = [dataObj length];
                NSLog(@"prepare to send %d bytes (image) to server", size);
                NSInteger bytesWritten, total = 0;
                while (size - total > 0) {
                    bytesWritten = [imageOutStream write:buf maxLength:size];
                    if (bytesWritten > 0) {
                        total += bytesWritten;
                        buf += bytesWritten;
                        NSLog(@"sent %d bytes", bytesWritten);
                    } else {
                        // retry
                        if (count < 5) {
                            count++;
                            NSLog(@"cannot write to socket, more bytes to be sent, retrying %d", count);
                            continue;
                        } 
                        break;
                    }
                }
                if (count >= 5) {
                    /* pop up an error message when this happen */
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Problem sharing screen with remote clients" 
                                                                   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    [alert release];
                }
                //[imageToUpload release]; <-- TODO: find out when/where to release this
                [imageOutStream close];
                [imageOutStream release];
                app_state = NORMAL;
            } else if ([[dict valueForKey:@"request"] isEqualToString:@"error"]) {
                if ([dict valueForKey:@"error_code"] != nil) {
                    NSString *errorCodeStr = [dict valueForKey:@"reason"];
                    if ([errorCodeStr intValue] == ERROR_SESSION_EXISTED) {
                        NSLog(@"error - trying to start an existing session");
                        /* pop warning dialog then close the open sockets, may need to wipe the canvas clean too */
                        UIAlertView *alert;
                        alert = [[UIAlertView alloc] initWithTitle:nil message:@"Session already existed, please start another one" 
                                                     delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                        [alert show];
                        [alert release];
                        /* TODO: check if ios needs to send leaveSesion to server so server can close socket on their side 
                                 flex code does that, does the close socket on my side will be sufficient to alert the server
                         */
                        [connectionButton setImage:[UIImage imageNamed:@"cloud_disconnect"] forState:UIControlStateNormal];
                        connected = FALSE;
                        [inputStream close];
                        [outputStream close];
                    }
                } else {
                    NSLog(@"malformed json request");
                }
            } else if ([[dict valueForKey:@"request"] isEqualToString:@"updateData"]) {
                NSLog(@"getting updateData request");
                if (app_state != INSTANT_SHARE) {
                    if ([dict valueForKey:@"user"] != nil) {
                        userName = [dict valueForKey:@"user"];  // TODO: retain probably not needed
                        if ([dict valueForKey:@"data"] != nil) {
                            //NSString *colorStr = [dict valueForKey:@"color"];
                            //NSMutableString *colorStr = [[NSMutableString alloc] init];
                            NSDictionary *colorValue = [dict valueForKey:@"color"];
                            CGColor_t remoteColor;
                            remoteColor.r = ([[colorValue valueForKey:@"r"] floatValue] / 255.0f);
                            remoteColor.g = ([[colorValue valueForKey:@"g"] floatValue] / 255.0f);
                            remoteColor.b = ([[colorValue valueForKey:@"b"] floatValue] / 255.0f);
                            NSMutableArray *tempArray = [[NSMutableArray alloc] init];
                            DrawContext *cxt = [[DrawContext alloc] init];
                            NSArray *dataArray = (NSArray *)[dict valueForKey:@"data"];
                            /* Add the last point receive previously if there is one to the array */
                            pointValue = [lastPointFromUser valueForKey:userName];
                            if (pointValue != nil) {
                                [tempArray addObject:pointValue];
                            }
                            NSLog(@"there are %d element in the array\n", [dataArray count]);
                            for (j = 0; j < [dataArray count]; j++) {
                                NSDictionary *element = (NSDictionary *)[dataArray objectAtIndex:j];
                                point.x = [[element valueForKey:@"x"] doubleValue];
                                point.y = [[element valueForKey:@"y"] doubleValue];
                                pointValue = [NSValue valueWithCGPoint:point];
                                [tempArray addObject:pointValue];
                            }
                            [cxt setColorContext:remoteColor];
                            cxt.dataPointArray = tempArray;
                            [lastPointFromUser setValue:[tempArray lastObject] forKey:userName];
                            [tempArray release];
                            /* if it's still in join state, buffer the cxt data, for later display */
                            if (netOp == JOIN_SESSION) {
                                NSLog(@"in join state, buffering cxt data");
                                [joinStateBuffer addObject:cxt];
                            } else {
                                [self performSelectorOnMainThread:@selector(drawUpdate:) withObject:cxt waitUntilDone:NO];
                            }
                            [cxt release];
                        }
                    } else {
                        /* do we even care who the user is? maybe we don't */
                        NSLog(@"ERROR: received drawing update from nil user, disregard the update\n");
                    }
                } else {
                    NSLog(@"in instant share mode, ignore all updates from remote\n");
                }
            }
        }
        [dict release];
    }
    [inputArray release];
    [jsonParser release];
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent 
{
    int len = 0;
    NSMutableString *jsonString = [[NSMutableString alloc] initWithCapacity:40];
    
	switch (streamEvent) {
        case NSStreamEventHasBytesAvailable:
            if (theStream == inputStream) {
                /* allocate a page size, more than enough to hold a single update */
                uint8_t buffer[4096];  
                while ([inputStream hasBytesAvailable]) {
                    /* updates coming from the server */
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        /* TODO: check if input is retained by the nsoperation initWithTarget:selector:object function, if this is then need to release input */
                        NSString *input = [[NSString alloc] initWithBytes:buffer length:len encoding:NSASCIIStringEncoding];
                        if (nil != input) {
                            NSInvocationOperation* op = [[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(receiveDataFromServer:) object:input] autorelease];
                            [fromServerQ addOperation:op];
                            [input release];
                        }
                    }
                }
            } else if (theStream == imageInStream) {
                if (rawData == nil) {
                    rawData = [[NSMutableData alloc] init];
                }
                /* okay to perform the receive function in the same thread, 
                   client can't do anything until it receives the background image
                 */
                uint8_t buffer[1024]; 
                while ([inputStream hasBytesAvailable]) {
                    len = [imageInStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        [rawData appendBytes:(const void *)buffer length:len];
                        rawBytesRead += len;
                    }
                    NSLog(@"received %d bytes from server, total bytes received so far is %d", len, rawBytesRead);
                    if (rawBytesRead >= fileSize) {
                        /* we are done, update the view with image we just received */
                        NSLog(@"we are done, received total of %d bytes, expects %d bytes", rawBytesRead, fileSize);
                        [activityIndicator stopAnimating];
                        activityIndicator.hidden = YES;
                        [self updateImageView:rawData];
                        [rawData release];
                        rawData = nil;
                        rawBytesRead = 0;
                    }
                }
            }
        break;
        case NSStreamEventOpenCompleted:
            if (theStream == imageOutStream) {
                NSLog(@"imageOutStream opened");
                if (app_state != INSTANT_SHARE) {
                    //UIGraphicsBeginImageContext(self.view.frame.size);
                    UIGraphicsBeginImageContext(self.view.bounds.size);
                    Draw2View * drawView = (Draw2View*)self.view;
                    imageToUpload = drawView.image;
                    [imageToUpload retain]; // TODO: check this line later
                    NSData *dataObj = UIImageJPEGRepresentation(imageToUpload, 1.0);
                    NSInteger size = [dataObj length];
                    /* send file size to the server */
                    [jsonString appendString:@"{\"request\":\"ackGetState\", \"clientHost\":"];
                    [jsonString appendFormat:@"\"%@\"", clientHost];
                    [jsonString appendString:@", \"clientPort\": "];
                    [jsonString appendFormat:@"%d", clientPort];
                    [jsonString appendString:@", \"length\": "];
                    [jsonString appendFormat:@"%d", size];
                    [jsonString appendString:@", \"sessionID\": "];
                    [jsonString appendFormat:@"\"%@\"", self.sessionID];
                    [jsonString appendString:@"}\n"];
                    NSLog(@"json string:\n%@", jsonString);
                    NSData *jsonData = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
                    NSLog(@"size of image to be uploaded is %d", size);
                    [outputStream write:[jsonData bytes] maxLength:[jsonData length]];
                    [jsonData release];
                }
            } else if (theStream == imageInStream) {
                NSLog(@"imageInStream opened");
                if (app_state != INSTANT_SHARE) {
                    netOp = JOIN_SESSION;
                    /* send ACK */
                    [jsonString appendString:@"{\"request\":\"ackReturnState\""];
                    [jsonString appendString:@", \"sessionID\": "];
                    [jsonString appendFormat:@"\"%@\"", self.sessionID];
                    [jsonString appendString:@"}\n"];
                    NSLog(@"json string:\n%@", jsonString);
                    NSData *jsonData = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
                    [outputStream write:[jsonData bytes] maxLength:[jsonData length]];
                    [jsonData release];
                }
            } else if (theStream == inputStream) {
                NSLog(@"inutStream is opened");
                /* TODO:
                 1. change the connection icon to connect 
                 */
                
            } else if (theStream == outputStream) {
                NSLog(@"outputStream is opened");
            }
        break;
        case NSStreamEventErrorOccurred:
            NSLog(@"Socket error string: %@", [[theStream streamError] localizedDescription]);
            if (theStream == outputStream || theStream == inputStream) {
                connected = FALSE;
                [connectionButton setImage:[UIImage imageNamed:@"cloud_disconnect"] forState:UIControlStateNormal];
                [inputStream close];
                [outputStream close];
                
                [outputStream close];
                [inputStream close];
                [connectionButton setImage:[UIImage imageNamed:@"cloud_disconnect"] forState:UIControlStateNormal];
                UIAlertView *alert;
                alert = [[UIAlertView alloc] initWithTitle:nil message:@"Network connection failed,\nplease try again"
                                                  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
        break;
    }
    [jsonString release];
}

- (void) updateImageView:(NSData *)imageData 
{
    int i;
    DrawContext *cxt;
    Draw2View * imgView;
    UIImage *img = [[UIImage alloc] initWithData:imageData];
    
    imgView = (Draw2View *)self.view;
    imgView.image = img;
    
    NSLog(@"done updating the background image");
    
    /* debug save the image to album */
    //UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
    
    /* can start drawing */
    mode = DRAW;
    //[img release];
    [imageInStream close];
    [imageInStream release];
    
    if (app_state != INSTANT_SHARE) {
        /* display the bufferred data */
        for (i = 0; i < [joinStateBuffer count]; i++) {
            cxt = [joinStateBuffer objectAtIndex:i];
            [self drawUpdate:cxt];
        }
    }
    app_state = NORMAL;
    netOp = NONE;
    [joinStateBuffer removeAllObjects];
    NSLog(@"input stream status is %d", [inputStream streamStatus]);
    NSLog(@"output stream status is %d", [outputStream streamStatus]);
}

- (void) uploadTask
{
    CGColor_t colorToUpload;
    int size, i, max_range;
    UIColor *color_value;
    CGPoint point;
    NSMutableString *coordValues = [[NSMutableString alloc] initWithCapacity:20];
    NSMutableString *jsonString = [[NSMutableString alloc] initWithCapacity:40];
    
    size = [points count];
    //NSLog(@"sending coordinates from index %d to %d", readIndex, size);
    if (size > readIndex) {
        /* due to array indexing we always get one last end of line delimiter in
           this calculation, so if size - readIndex is equal to 1, that could be
           the delimiter, don't need to send if that's the case
         */
        if (size - readIndex == 1) {
            point = [[points objectAtIndex:readIndex] CGPointValue];
            if (point.x == -1.0 && point.y == -1.0) {
                readIndex++; // skip over this one, so the new line starts off correctly
                [coordValues release];
                [jsonString release];
                return;
            }
        }
        /* use the current color scheme */
        if ([markerArray count] == 0) {
            /* there are points to send */
            if (size - readIndex >= 15) {
                max_range = readIndex + 15;
            } else {
                max_range = size;
            }
            const CGFloat *c = CGColorGetComponents(drawingColor.CGColor);
            if (CGColorSpaceGetModel(CGColorGetColorSpace(drawingColor.CGColor)) == kCGColorSpaceModelMonochrome) {
                colorToUpload.r = c[0] * 255;
                colorToUpload.g = c[0] * 255;
                colorToUpload.b = c[0] * 255;
            } else {
                colorToUpload.r = c[0] * 255;
                colorToUpload.g = c[1] * 255;
                colorToUpload.b = c[2] * 255;
            }
        } else {
            /* always get the first element, since it will be pop after it's done */
            NSValue *value = [markerArray objectAtIndex:0];
            color_marker_t marker;
            [value getValue:&marker];
            color_value = marker.color;
            const CGFloat *c = CGColorGetComponents(color_value.CGColor);
            if (CGColorSpaceGetModel(CGColorGetColorSpace(color_value.CGColor)) == kCGColorSpaceModelMonochrome) {
                colorToUpload.r = c[0] * 255;
                colorToUpload.g = c[0] * 255;
                colorToUpload.b = c[0] * 255;
            } else {
                colorToUpload.r = c[0] * 255;
                colorToUpload.g = c[1] * 255;
                colorToUpload.b = c[2] * 255;
            }
            if (marker.boundary - readIndex >= 15) {
                max_range = readIndex + 15;
            } else {
                max_range = marker.boundary;
                /* TODO: maybe release the market.color here */
                [marker.color release];
                [markerArray removeObjectAtIndex:0];
            }
        }
        for (i = readIndex; i < max_range; i++) {
            if (i > readIndex) {
                [coordValues appendString:@", "];
            }
            point = [[points objectAtIndex:i] CGPointValue];
            [coordValues appendString:@"{\"x\":"];
            [coordValues appendString:[NSString stringWithFormat:@"%f, ", point.x]];
            [coordValues appendString:@"\"y\":"];
            [coordValues appendString:[NSString stringWithFormat:@"%f", point.y]];
            [coordValues appendString:@"}"];
        }
        [jsonString appendString:@"{\"request\":\"updateData\", \"user\":"];
        [jsonString appendFormat:@"\"%@\"", self.userID];
        [jsonString appendString:@", \"color\": {\"r\":"];
        [jsonString appendFormat:@"%f, \"g\":", colorToUpload.r];
        [jsonString appendFormat:@"%f, \"b\":", colorToUpload.g];
        [jsonString appendFormat:@"%f}", colorToUpload.b];
        //[jsonString appendString:@", \"color\": "];
        //[jsonString appendFormat:@"\"%@\"", [self getColorScheme:color_value]];
        //[jsonString appendString:@", \"range\": "];
        //[jsonString appendFormat:@"\"%d - %d\"", readIndex, max_range];
        //[jsonString appendString:[NSString stringWithFormat:@"%f", [dateTime timeIntervalSinceReferenceDate]]];
        //[jsonString appendFormat:@"%f", [dateTime timeIntervalSinceReferenceDate]];
        [jsonString appendString:@", \"sessionID\": "];
        [jsonString appendFormat:@"\"%@\"", self.sessionID];
        [jsonString appendString:@", \"data\": ["];
        [jsonString appendString:coordValues];
        [jsonString appendString:@"]}\n"];
        NSLog(@"json string:\n%@", jsonString);
        NSData *jsonData = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
        [outputStream write:[jsonData bytes] maxLength:[jsonData length]];
        readIndex = max_range - 1;
        [jsonData release];
    }
    [coordValues release];
    [jsonString release];
}

// timer function to send coordinate points to the server
- (void)sendCoordinates:(NSTimer*)theTimer
{
    int length;
    NSMutableString *jsonString;
    
    if (connected) {
        NSInvocationOperation* op = [[[NSInvocationOperation alloc] initWithTarget:self selector:@selector(uploadTask) object:nil] autorelease];
    
        [toServerQ addOperation:op];
    } else {
        if ([outputStream streamStatus] == NSStreamStatusOpen) {
            connected = TRUE;
            [connectionButton setImage:[UIImage imageNamed:@"cloud_connect"] forState:UIControlStateNormal];
            /* first time output stream is ready to write */
            jsonString = [[NSMutableString alloc] initWithString:@"{\"request\":\"connect\", \"user\":"];
            [jsonString appendFormat:@"\"%@\"", self.userID];
            [jsonString appendString:@", \"sessionID\": "];
            [jsonString appendFormat:@"\"%@\"}", self.sessionID];
            NSLog(@"debug: jsonString: %@\nstream status %d", jsonString, [outputStream streamStatus]);
            NSData *jsonData = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
            
            length = [outputStream write:[jsonData bytes] maxLength:[jsonData length]];
            if (length < 0) {
                /* something wrong with the socket connection, exit */
                connected = FALSE;
                [outputStream close];
                [inputStream close];
                //[inputStream release];
                //[outputStream release];
                [connectionButton setImage:[UIImage imageNamed:@"cloud_disconnect"] forState:UIControlStateNormal];
                [self dismissModalViewControllerAnimated:YES];
                UIAlertView *alert;
                alert = [[UIAlertView alloc] initWithTitle:nil message:@"Network connection failed,\nplease try again"
                                                  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [alert release];
            }
            [jsonData release];
            [jsonString release];
        }
    }
}


- (void) viewWillAppear:(BOOL)animated 
{
    NSLog(@"entering viewWillAppear\n");
    CGAffineTransform xform = CGAffineTransformMakeRotation(M_PI/2.0);
    self.view.transform = xform;
    CGRect contentRect = CGRectMake(0,0, 1024, 768);
	self.view.bounds = contentRect;
}

- (void) initApplication
{
    app_state = NORMAL;
    mode = DRAW; 
    readIndex = 0;
    rawBytesRead = 0;
    connected = FALSE;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"entering viewDidLoad\n");
    [self initApplication]; // this method could be re-use when app resume from bg
    self.drawingColor = [UIColor blackColor];
    self.currentColor = [UIColor blackColor];
    UIView *btnView = [[UIView alloc] initWithFrame:colorButton.bounds];
    btnView.backgroundColor = drawingColor;
    btnView.userInteractionEnabled = NO;
    btnView.layer.cornerRadius = 10.0f;
    btnView.layer.masksToBounds = YES;
    [colorButton addSubview:btnView];
    [btnView release];
    lastPointFromUser = [[NSMutableDictionary alloc] init];
    points = [[NSMutableArray alloc] init];
    data = [[NSMutableData alloc] init];
    toServerQ = [[NSOperationQueue alloc] init];
    fromServerQ = [[NSOperationQueue alloc] init];
    userID = [[NSString alloc] initWithFormat:@"iOS_%u", arc4random()];
    sessionID = nil;
    joinStateBuffer = [[NSMutableArray alloc] init];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    saveButton.showsTouchWhenHighlighted = YES;
    netOp = NONE;
    firstTime = TRUE;
    activityIndicator.hidden = YES;
    
#if 0    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    //[self setShouldPlaySounds:[defaults boolForKey:@"play_sounds_preference"]];
    NSString *access_mode = [defaults stringForKey:@"mywb_access_preference"];
    if ([access_mode isEqualToString:@"Full"]) {
        NSLog(@"access_mode = %@", access_mode);
    } else {
        /* disable camera and save functionality */
        NSLog(@"could not read access mode from preference, must be limited mode");
        cameraButton.userInteractionEnabled = NO;
        saveButton.userInteractionEnabled = NO;
    }
    

    /* attempting to set the access preference */
    [defaults setObject:[[NSString alloc] initWithString:@"Limited"] forKey:@"mywb_access_preference"];
    
    /* access this value again */
    access_mode = [defaults stringForKey:@"mywb_access_preference"];
    if (access_mode != nil) {
        NSLog(@"access_mode = %@", access_mode);
    } else {
        NSLog(@"failed to read access mode from preference again");
    }
#endif
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        NSLog(@"In landscape right mode");
        return YES;
    } 
     
    return NO;
}

/*
- (IBAction) rePlay:(id)sender
{
    int i;
    CGPoint point;
    NSKeyedUnarchiver *unarchiver;
    
    unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    points = [[unarchiver decodeObjectForKey:@"drawingPath"] retain];
    [unarchiver finishDecoding];
    [unarchiver release];
    NSLog(@"there are %d points in the array\n", [points count]);
    
    for (i = 0; i < [points count]; i++) {
        point = [[points objectAtIndex:i] CGPointValue];
        NSLog(@"%f,%f, ", point.x, point.y);
    }
    [self.view drawRect:[self.view bounds]];
}

- (IBAction) serializeData:(id)sender
{
    NSKeyedArchiver *archiver;
    
    if (points) {
        //data = [NSMutableData data];
        archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        [archiver encodeObject:points forKey:@"drawingPath"];
        [archiver finishEncoding];
        [archiver release];
        [points removeAllObjects];
        
        NSLog(@"there are %d points in the array\n", [points count]);
        [self.view drawRect:[self.view bounds]];
    }
}
*/

- (BOOL) canMakePurchase
{
    if ([SKPaymentQueue canMakePayments]) {
        return YES;
    } else {
        return NO;
    }
}

- (void) upgradeToFullVersion
{
    if ([self canMakePurchase]) {
        NSSet *productIdentifiers = [NSSet setWithObject:kInAppPurchaseUpgradeProductId];
        
        SKProductsRequest *productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIdentifiers];
        productsRequest.delegate = self;
        [productsRequest start];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Premium feature requires In-App Purchase\nPlease turn on In-App Purchases in your account setting" 
                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
}

- (void) productsRequest:(SKProductsRequest *)request
     didReceiveResponse:(SKProductsResponse *)response
{
    NSArray *products = response.products;
    SKProduct *productID;
    
    productID = [products count] == 1 ? [[products objectAtIndex:0] retain] : nil;
    if (productID)
    {
        NSLog(@"Product title: %@" , productID.localizedTitle);
        NSLog(@"Product description: %@" , productID.localizedDescription);
        NSLog(@"Product price: %@" , productID.price);
        NSLog(@"Product id: %@" , productID.productIdentifier);
        
        /* received product detail from app store, pops up an alert to confirm the purchase */
        NSString *confirmationMsg = [[NSString alloc] initWithFormat:@"Do you want to upgrae to %@ for $%@ to access the premium feature", productID.localizedDescription, productID.price]; 
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm Your In-App Purchase" message:confirmationMsg 
                                                       delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Buy", nil];
        alert.tag = PURCHASE_ALERT;
        [alert show];
        [alert release];
        [productID release];
        [confirmationMsg release];
    }
    [request autorelease];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == PURCHASE_ALERT) {
        SKPayment *payment = [SKPayment paymentWithProductIdentifier:kInAppPurchaseUpgradeProductId];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    } else if (alertView.tag == DELETE_ALERT) {
        if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Confirm"]) {
            NSLog(@"debug: clean up the canvas");
            Draw2View *imgView;
            /* clean the canvas, remove all array points and bg image */
            [points removeAllObjects];
            [lastPointFromUser removeAllObjects];
            [markerArray removeAllObjects];
            readIndex = 0;
            imgView = (Draw2View *)self.view;
            imgView.image = nil;
            if (connected) {
                NSMutableString *jsonString = [[NSMutableString alloc] initWithCapacity:40];
                [jsonString appendString:@"{\"request\":\"trash\", \"sessionID\":"];
                [jsonString appendFormat:@"\"%@\"}", self.sessionID];
                NSLog(@"json string:\n%@", jsonString);
                NSData *jsonData = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
                [outputStream write:[jsonData bytes] maxLength:[jsonData length]];
                [jsonData release];
                [jsonString release];
            }
        }
    }
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction
{
    if ([transaction.payment.productIdentifier isEqualToString:kInAppPurchaseUpgradeProductId])
    {
        // save the transaction receipt to disk
        [[NSUserDefaults standardUserDefaults] setValue:transaction.transactionReceipt forKey:@"InAppPurchaseTransactionReceipt"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)provideContent:(NSString *)productId
{
    if ([productId isEqualToString:kInAppPurchaseUpgradeProductId])
    {   
        /* change to full access in preference */
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        [standardUserDefaults setObject:[NSString stringWithString:@"Full"] forKey:@"mywb_access_preference"];
        cameraButton.userInteractionEnabled = YES;
        saveButton.userInteractionEnabled = YES;
    }
}

- (void) completeTransaction: (SKPaymentTransaction *)transaction
{
    [self recordTransaction:transaction];
    [self provideContent:transaction.payment.productIdentifier];
    // Remove the transaction from the payment queue.
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
    [self recordTransaction:transaction];
    [self provideContent:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Oops, encounter error during payment transaction");
        /* TODO: figure out how to present the error to user */
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray*)transactions
{
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void) cleanUpSession
{
    rawBytesRead = 0;
    readIndex = 0;
    firstTime = FALSE;
    [timer invalidate];
    connected = FALSE;
    [inputStream close];
    [outputStream close];
    app_state = NORMAL;
    [activityIndicator stopAnimating];
    activityIndicator.hidden = YES;
    [connectionButton setImage:[UIImage imageNamed:@"cloud_disconnect"] forState:UIControlStateNormal];
    /* flush the arrays */
    [points removeAllObjects];
    [markerArray removeAllObjects];
    [lastPointFromUser removeAllObjects];
}

- (void) suspendForegroundTasks
{
    //Draw2View *imgView = (Draw2View *)self.view;
    //imgView.image = nil;

    [self cleanUpSession];
}

- (void) resumeFromBackground
{
    int length;
    
    NSLog(@"entering resumeFromBackground");
    if (self.sessionID == nil) {
        return;
    }
    connected = TRUE;
    [connectionButton setImage:[UIImage imageNamed:@"cloud_connect"] forState:UIControlStateNormal];
    [self initNetworkCommunication:self.serverIP withPort:8080 role:GENERAL];
    NSMutableString *jsonString = [[NSMutableString alloc] initWithString:@"{\"request\":\"joinSession\", \"user\":"];
    [jsonString appendFormat:@"\"%@\"", self.userID];
    [jsonString appendString:@", \"sessionID\": "];
    [jsonString appendFormat:@"\"%@\"}", self.sessionID];
    if (connected) {
        NSLog(@"debug: stream status in resume from background %d", [outputStream streamStatus]);
        NSData *jsonData = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
        length = [outputStream write:[jsonData bytes] maxLength:[jsonData length]];
        if (length < 0) {
            /* something wrong with the socket connection, exit */
            connected = FALSE;
            [outputStream close];
            [inputStream close];
            //[inputStream release];
            //[outputStream release];
            [connectionButton setImage:[UIImage imageNamed:@"cloud_disconnect"] forState:UIControlStateNormal];
            UIAlertView *alert;
            alert = [[UIAlertView alloc] initWithTitle:nil message:@"Failed to rejoin existing session\nplease start/join session later"
                                              delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            [jsonData release];
            [jsonString release];
            return;
        }
        SEL sendCoordinates = @selector(sendCoordinates:);
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:sendCoordinates userInfo:nil repeats:TRUE];
        [jsonData release];
    }
    [jsonString release];
}

- (void) userInputFinish:(NSString *)server_ip 
         withSessionID:(NSString *)session_id 
         withUserName:(NSString *)user_name
         isConfirm:(Boolean)confirm
{
        
    [self dismissModalViewControllerAnimated:YES];
    if (confirm && netOp != NONE) {
        self.serverIP = server_ip;
        self.sessionID = session_id;
        
        [self initNetworkCommunication:self.serverIP withPort:8080 role:GENERAL];
/*
        if (netOp == START_SESSION) {
            jsonString = [[NSMutableString alloc] initWithString:@"{\"request\":\"startSession\", \"user\":"];
            [jsonString appendFormat:@"\"%@\"", self.userID];
            [jsonString appendString:@", \"sessionID\": "];
            [jsonString appendFormat:@"\"%@\"}", self.sessionID];
        } else {
            jsonString = [[NSMutableString alloc] initWithString:@"{\"request\":\"joinSession\", \"user\":"];
            [jsonString appendFormat:@"\"%@\"", self.userID];
            [jsonString appendString:@", \"sessionID\": "];
            [jsonString appendFormat:@"\"%@\"}", self.sessionID];
        }
*/
        mode = DRAW;
        netOp = NONE;
        SEL sendCoordinates = @selector(sendCoordinates:);
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:sendCoordinates userInfo:nil repeats:TRUE];
    }
}

- (IBAction) startSession:(id)sender
{
    Draw2InputViewController *modalView = [[Draw2InputViewController alloc] init];
    modalView.delegate = self;
    modalView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    modalView.modalPresentationStyle = UIModalPresentationFormSheet;
    netOp = START_SESSION;
    [self presentModalViewController:modalView animated:YES];
    [modalView release];
}

- (IBAction) joinSession:(id)sender
{
    Draw2InputViewController *modalView = [[Draw2InputViewController alloc] init];
    modalView.delegate = self;
    modalView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    modalView.modalPresentationStyle = UIModalPresentationFormSheet;
    netOp = JOIN_SESSION;
    [self presentModalViewController:modalView animated:YES];
    [modalView release];
}

- (IBAction) connectToServer:(id)sender
{
    if (connected) {
        [self cleanUpSession];
        /* TODO: do we wipe out the canvas here ?? */
    } else {
        Draw2InputViewController *modalView = [[Draw2InputViewController alloc] init];
        modalView.delegate = self;
        modalView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        modalView.modalPresentationStyle = UIModalPresentationFormSheet;
        netOp = CONNECT_SESSION;
        [self presentModalViewController:modalView animated:YES];
        [modalView release];
    }
}

- (IBAction) sendPoints:(id)sender
{
    int i;
    CGPoint point;
    NSMutableString *coordValues = [[NSMutableString alloc] initWithCapacity:20];
    NSMutableString *jsonString = [[NSMutableString alloc] initWithCapacity:40];
    dateTime = [[NSDate alloc] init];
    
    NSLog(@"dateTime native representation: %f", [dateTime timeIntervalSinceReferenceDate]);
    for (i = 0; i < [points count]; i++) {
        if (i > 0) {
            [coordValues appendString:@", "];
        }
        point = [[points objectAtIndex:i] CGPointValue];
        [coordValues appendString:@"{\"x\":"];
        [coordValues appendString:[NSString stringWithFormat:@"%f, ", point.x]];
        [coordValues appendString:@"\"y\":"];
        [coordValues appendString:[NSString stringWithFormat:@"%f", point.y]];
        [coordValues appendString:@"}"];
    }
    [jsonString appendString:@"{\"timestamp\": "];
    //[jsonString appendString:[NSString stringWithFormat:@"%f", [dateTime timeIntervalSinceReferenceDate]]];
    [jsonString appendFormat:@"%f", [dateTime timeIntervalSinceReferenceDate]];
    [jsonString appendString:@", \"sessionID\": "];
    [jsonString appendFormat:@"\"%@\"", sessionID];
    [jsonString appendString:@", \"data\": ["];
    [jsonString appendString:coordValues];
    [jsonString appendString:@"]}"];
    NSLog(@"json string:\n%@", jsonString);
    
    NSURL *url = [NSURL URLWithString:@"http://localhost:8888/draw/postData"];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request appendPostData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setDelegate:self];
    [request startAsynchronous];
    
    /* clear points here, get the points from server to reconstruct the whole drawing */
    NSLog(@"sending %d points to the server\n", [points count]);
    [points removeAllObjects];
    [self.view drawRect:[self.view bounds]];
    [coordValues release];
    [jsonString release];
    [dateTime release];
}

- (IBAction) getPoints:(id)sender
{
    NSMutableString *URL = [[NSMutableString alloc] initWithString:@"http://localhost:8888/draw/getAllData?sessionID="];
    [URL appendString:sessionID];
    [URL appendFormat:@"&timestamp=%f", [dateTime timeIntervalSinceReferenceDate]];
    NSLog(@"GET URL: %@", URL);
    NSURL *url = [NSURL URLWithString:URL];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setRequestMethod:@"GET"];
    [request setDelegate:self];
    [request startAsynchronous];
    [URL release];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    int i, j;
    CGPoint point;
    NSValue *pointValue;
    // Use when fetching text data
    SBJsonParser *jsonParser = [[SBJsonParser alloc] init];
    NSString *responseString = [request responseString];
    NSLog(@"received http response: %@", responseString);
    NSDictionary *dict = (NSDictionary *)[jsonParser objectWithString:responseString];
    if ([dict valueForKey:@"sessionID"] != nil) {
        sessionID = [[dict valueForKey:@"sessionID"] retain];
    }
    NSLog(@"sessionID = %@", sessionID);
    if ([dict valueForKey:@"data"] != nil) {
        NSArray *dataArray = (NSArray *)[dict valueForKey:@"data"];
        for (j = 0; j < [dataArray count]; j++) {
            NSDictionary *element = (NSDictionary *)[dataArray objectAtIndex:j];
            NSArray *coordArray = [element valueForKey:@"data"];
            NSLog(@"there are %d element in the array\n", [coordArray count]);
            for (i = 0; i < [coordArray count]; i++) {
                NSDictionary *coordinatePt = (NSDictionary *)[coordArray objectAtIndex:i];
                point.x = [[coordinatePt valueForKey:@"x"] doubleValue];
                point.y = [[coordinatePt valueForKey:@"y"] doubleValue];
                pointValue = [NSValue valueWithCGPoint:point];
                [points addObject:pointValue];
                NSLog(@"%f, %f", [pointValue CGPointValue].x, [pointValue CGPointValue].y);
            }
        }
        [self.view drawRect:[self.view bounds]];
    }
    [jsonParser release];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSError *error = [request error];
    NSLog(@"http request return error, error description: %@", [error localizedDescription]);
}

-(void) disMissSaveDialog 
{
    if (popOver != nil) {
        [popOver dismissPopoverAnimated:NO];
        [popOver release];
    }
}

-(void) disMissImportDialog 
{
    if (popOver != nil) {
        [popOver dismissPopoverAnimated:NO];
        [popOver release];
    }
}

-(IBAction) saveDrawing:(id)sender
{
    CGFloat height;
    UIButton *sButton = (UIButton *)sender;
    Draw2View * myView = (Draw2View *)self.view;
    
    if ([self isFullVersion] || isDEBUG) {
        Draw2SaveController *saveController = [[Draw2SaveController alloc] initWithStyle:UITableViewStylePlain];
        saveController.delegate = self;
        [saveController sendPictureToView:myView.image];
        popOver = [[UIPopoverController alloc] initWithContentViewController:saveController];
        [saveController release];  //TODO: figure out why release here causes a crash
        height = 40 * 2; // 2 is the number of rows we have for this pop up
        popOver.popoverContentSize = CGSizeMake(240, height);
        /*
        [popOver presentPopoverFromRect:CGRectMake(saveButton.frame.origin.x + saveButton.frame.size.width, (saveButton.frame.origin.y*2 + saveButton.frame.size.height)/2, 1, 1) 
                                 inView:self.view
               permittedArrowDirections:UIPopoverArrowDirectionLeft 
                               animated:YES];
         */
        [popOver presentPopoverFromRect:sButton.frame 
                                 inView:self.view
               permittedArrowDirections:UIPopoverArrowDirectionAny 
                               animated:YES];
    } else {
        [self upgradeToFullVersion];
    }
}

- (IBAction) deleteDrawing:(id)sender
{
    UIAlertView *alert;
    alert = [[UIAlertView alloc] initWithTitle:nil message:@"Are you sure? Clicking Confirm will clear the board for everyone!" 
                                      delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
    alert.tag = DELETE_ALERT;
    [alert show];
    [alert release];
}

-(IBAction) takePicture:(id)sender
{
    CGFloat height;
    UIButton *importButton = (UIButton *)sender;
    if ([self isFullVersion] || isDEBUG) {
        ImportViewController *importController = [[ImportViewController alloc] initWithStyle:UITableViewStylePlain];
        importController.parent = self;
        importController.delegate = self;
        popOver = [[UIPopoverController alloc] initWithContentViewController:importController];
        height = 40*2;
        popOver.popoverContentSize = CGSizeMake(240, height);
        [popOver presentPopoverFromRect:importButton.frame 
                                 inView:self.view
               permittedArrowDirections:UIPopoverArrowDirectionAny 
                               animated:YES];
        [importController release];
    } else {
        [self upgradeToFullVersion];
    }
}

- (Boolean) isFullVersion
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *access_mode = [defaults stringForKey:@"mywb_access_preference"];
    if ([access_mode isEqualToString:@"Full"]) {
        NSLog(@"access_mode = %@", access_mode);
        return TRUE;
    } else {
        /* disable camera and save functionality */
        NSLog(@"could not read access mode from preference, must be limited mode");
        return FALSE;
    }
}

- (void) screenShare:(UIImage*)image
{
    NSMutableString *jsonString = [[NSMutableString alloc] initWithCapacity:40];
    
    if (connected) {
        app_state = INSTANT_SHARE;
        imageToUpload = image;
        [imageToUpload retain]; // TODO: check this line later
        NSData *dataObj = UIImageJPEGRepresentation(imageToUpload, 1.0);
        NSInteger size = [dataObj length];
        /* send file size to the server */
        [jsonString appendString:@"{\"request\":\"startBulkSend\", \"length\":"];
        [jsonString appendFormat:@"\"%d\"", size];
        [jsonString appendString:@", \"sessionID\": "];
        [jsonString appendFormat:@"\"%@\"", self.sessionID];
        [jsonString appendString:@"}\n"];
        NSLog(@"json string:\n%@", jsonString);
        NSData *jsonData = [[NSData alloc] initWithData:[jsonString dataUsingEncoding:NSASCIIStringEncoding]];
        [outputStream write:[jsonData bytes] maxLength:[jsonData length]];
        [jsonData release];
        [jsonString release];
    }
}

- (void) cameraCaptureDidFinished:(UIImage *)capturedImage
{
    [popOver dismissPopoverAnimated:NO];
    
    if (capturedImage != nil) {
        Draw2View * imgView;
        imgView = (Draw2View *)self.view;
        imgView.image = capturedImage;
        
    #if 0
        CGSize size = capturedImage.size;
        UIGraphicsBeginImageContext(self.view.bounds.size);
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextRotateCTM(context, M_PI/2.0);
        //CGContextTranslateCTM( context, 0.5f * size.width, 0.5f * size.height ) ;
        
        //[imgView.image drawInRect:CGRectMake(0, 0, size.width, size.height)];
        [imgView.image drawAtPoint:CGPointMake(0.0, 0.0)];
        UIGraphicsEndImageContext();
    #endif
        NSLog(@"image orientation displayed on screen is %d", [imgView.image imageOrientation]);
        
        /* share my screen with other participants */
        [self screenShare:capturedImage];
    }
}

/*
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    Draw2View * imgView;
    UIImage *photo = [[info valueForKey:UIImagePickerControllerOriginalImage] retain];
    imgView = (Draw2View *)self.view;
    imgView.image = photo;
    [photo release];
    
    // need to find a better place for the following lines due to loading image from photo album using ImportViewController class
    [popOver dismissPopoverAnimated:NO];
    [popOver release];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //[self dismissModalViewControllerAnimated:YES];
    [popOver dismissPopoverAnimated:NO];
    [popOver release];
}
 */

- (void) getImageFromImagePicker:(UIImage*)img
{
    Draw2View * imgView;
    imgView = (Draw2View *)self.view;
    imgView.image = img;
    // need to find a better place for the following lines due to loading image from photo album using ImportViewController class
    [popOver dismissPopoverAnimated:NO];
    [popOver release];
    /* share my screen */
    [self screenShare:img];
}

- (void)colorPickerViewController:(ColorPickerViewController *)colorPicker didSelectColor:(UIColor *)selected_color 
{
    int number;
    
    if (!markerArray) {
        markerArray = [[NSMutableArray alloc] init];
    }
    mode = DRAW;
    /* display the selected color on the button */
    UIView *btnView = [[UIView alloc] initWithFrame:colorButton.bounds];
    //btnView.alpha = 0.3f;
    btnView.backgroundColor = selected_color;
    btnView.userInteractionEnabled = NO;
    btnView.layer.cornerRadius = 10.0f;
    btnView.layer.masksToBounds = YES;
    /* remove the current uiview first */
    for (int i = 0; i < [[colorButton subviews] count]; i++) {
        [[[colorButton subviews] objectAtIndex:i] removeFromSuperview];
    }
    [colorButton addSubview:btnView];
    number = [points count];
    if (number > 0) { // color changed, record the last point where old color was used
        color_marker_t marker;
        marker.color = self.drawingColor;
        [marker.color retain];
        marker.boundary = number;
        NSValue *value = [NSValue valueWithBytes:&marker objCType:@encode(color_marker_t)];
        [markerArray addObject:value];
    }
    self.drawingColor = selected_color;
    self.currentColor = selected_color;
    [popOver dismissPopoverAnimated:NO];
    [popOver release];
    [btnView release];
    [eraseButton setImage:[UIImage imageNamed:@"eraser"] forState:UIControlStateNormal];
    //const CGFloat *c = CGColorGetComponents(selected_color.CGColor);
    //NSLog(@"selected color RGB value is %f:%f:%f\n", c[0], c[1], c[2]);
}

- (IBAction) changeColor:(id)sender
{
    // Make a new ColorPickerViewController with the interface defined in the referenced nib:
    ColorPickerViewController *colorPickerViewController = 
    [[ColorPickerViewController alloc] initWithNibName:@"ColorPickerViewController" bundle:nil];
    
    colorPickerViewController.delegate = self;
    [colorPickerViewController setInitXhairPosition:self.currentColor];
    popOver = [[UIPopoverController alloc] initWithContentViewController:colorPickerViewController];
    popOver.popoverContentSize = CGSizeMake(320, 460);
    [popOver presentPopoverFromRect:colorButton.frame 
                             inView:self.view
           permittedArrowDirections:UIPopoverArrowDirectionAny 
                           animated:YES];
    [colorPickerViewController release];
}

- (void) cancelColorPicker
{
    [popOver dismissPopoverAnimated:NO];
    [popOver release];
}

- (IBAction) setEdittingMode:(id)sender
{
    int number;
    UIColor *prev_color;
    
    UIButton *button = (UIButton *)sender;
    
    if ([button.titleLabel.text isEqualToString:@"Edit"]) {
        mode = DRAW;
        self.drawingColor = self.currentColor;
        [eraseButton setImage:[UIImage imageNamed:@"eraser"] forState:UIControlStateNormal];
        NSLog(@"Changing to editing mode");
    } else if ([button.titleLabel.text isEqualToString:@"Erase"]) {
        [eraseButton setImage:[UIImage imageNamed:@"eraser_pressed3"] forState:UIControlStateNormal];
        prev_color = self.drawingColor;
        mode = ERASE;
        NSLog(@"Changing to erasing mode");
        self.drawingColor = [UIColor whiteColor];
        number = [points count];
        if (number > 0) { // color changed, record the last point where old color was used
            color_marker_t marker;
            marker.color = [prev_color retain];
            marker.boundary = number;
            NSValue *value = [NSValue valueWithBytes:&marker objCType:@encode(color_marker_t)];
            [markerArray addObject:value];
        }
    }
}

- (void) setMode:(draw_mode_t)drawingMode
{
    mode = drawingMode;
}

- (draw_mode_t) getMode
{
    return mode;
}

- (Boolean) isConnected
{
    return connected;
}

- (Boolean) isFirstTime
{
    return firstTime;
}

- (void) setFirstTime:(Boolean)isFirstTime
{
    firstTime = isFirstTime;
}

@end
